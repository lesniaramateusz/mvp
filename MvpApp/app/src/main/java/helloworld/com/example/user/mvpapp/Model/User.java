package helloworld.com.example.user.mvpapp.Model;

import android.text.TextUtils;
import android.util.Patterns;

public class User {
    public String email, password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    String getEmail() {
        return email;
    }


    String getPassword() {
        return password;
    }


   public boolean emailIsValid() {
        return !TextUtils.isEmpty(getEmail()) && Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches();
    }

  public  boolean passwordIsValid() {
        return getPassword().length() > 5;
    }

}
