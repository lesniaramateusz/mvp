package helloworld.com.example.user.mvpapp.Presenter;

import helloworld.com.example.user.mvpapp.Model.User;
import helloworld.com.example.user.mvpapp.View.RegistrationView;

public class RegistrationPresenter implements IRegistrationPresenter {

    RegistrationView registrationView;

    public RegistrationPresenter(RegistrationView registrationView) {
        this.registrationView = registrationView;
    }



    @Override
    public void onRegistration(String email,String password) {
        User user = new User(email, password);
        boolean registrationSuccess = user.emailIsValid() && user.passwordIsValid();
        if (registrationSuccess)
            registrationView.onRegistrationResults("Registration success");
        else registrationView.onRegistrationResults("Registration Failed\nCheck Data");
    }
}
