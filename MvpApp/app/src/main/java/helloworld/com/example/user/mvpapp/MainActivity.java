package helloworld.com.example.user.mvpapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import helloworld.com.example.user.mvpapp.Presenter.IRegistrationPresenter;
import helloworld.com.example.user.mvpapp.Presenter.RegistrationPresenter;
import helloworld.com.example.user.mvpapp.View.RegistrationView;

public class MainActivity extends AppCompatActivity implements RegistrationView{
    EditText emailInput, passwordInput;
    Button btn_createAccount;
    IRegistrationPresenter registrationPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_createAccount = (Button)findViewById(R.id.btn_createAccount);
        emailInput = (EditText) findViewById(R.id.emailInput);
        passwordInput = (EditText) findViewById(R.id.passwordInput);
        registrationPresenter = new RegistrationPresenter(this);
        btn_createAccount.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                registrationPresenter.onRegistration(emailInput.getText().toString(),passwordInput.getText().toString());
            }


        });
    }

    @Override
    public void onRegistrationResults(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
}
