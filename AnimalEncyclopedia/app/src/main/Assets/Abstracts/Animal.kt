package Abstracts

abstract class Animal internal constructor(private var name: String) {
    fun getName(): String {
        return "$species $name"
    }

    abstract var birthType: String
    abstract var dietType: String


    private val species: String
        private get() = javaClass.simpleName
}